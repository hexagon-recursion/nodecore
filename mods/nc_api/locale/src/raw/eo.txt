msgid ""
msgstr ""
"PO-Revision-Date: 2021-06-19 11:38+0000\n"
"Last-Translator: Krzysztof Jeszke <krzysztofjesz@gmail.com>\n"
"Language-Team: Esperanto <http://nodecore.mine.nu/trans/projects/nodecore/"
"core/eo/>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.6.2\n"

msgid "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2021 de Aaron Suen <warr1024@@gmail.com>"

msgid "+"
msgstr "+"

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- \"Fornoj\" ne estas afero; malkovru elfandadon kun malfermaj flamoj."

msgid "- @1"
msgstr "- @1"

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr "- Ĉu ne povas fosi arbojn aŭ herbojn? Serĉu bastonojn en la baldakeno."

msgid "- Crafting is done by building recipes in-world."
msgstr "- Kreado estas farita per konstruado de receptoj en la mondo."

msgid "- DONE: @1"
msgstr "- FARITA: @1"

msgid "- Do not use F5 debug info; it will mislead you!"
msgstr "- Ne uzu F5-elpurigajn informojn; ĝi erarigos vin!"

msgid "- Trouble lighting a fire? Try using longer sticks, more tinder."
msgstr ""
"- Ĉu problemoj ekbruligas fajron? Provu uzi pli longajn bastonojn, pli da "
"tindro."

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr "- Faligu erojn sur la teron por krei stakajn nodojn. Ili ne kadukiĝas."

msgid "- Hold/repeat right-click on walls/ceilings barehanded to climb."
msgstr "- Tenu / ripetu dekstre alklaku murojn / plafonojn nudmane por grimpi."

msgid "- If a recipe exists, you will see a special particle effect."
msgstr "- Se recepto ekzistas, vi vidos specialan partiklan efikon."

msgid "- Items picked up try to fit into the current selected slot first."
msgstr "- Eroj reprenitaj unue penas enigi la nunan elektitan fendon."

msgid "- Larger recipes are usually more symmetrical."
msgstr "- Pli grandaj receptoj estas kutime pli simetriaj."

msgid "- Order and specific face of placement may matter for crafting."
msgstr "- Ordo kaj specifa vizaĝo de lokado eble gravas por kreado."

msgid "- Ores may be hidden, but revealed by subtle clues in terrain."
msgstr ""
"- Ercoj povas esti kaŝitaj, sed malkaŝitaj per subtilaj spuroj sur la tereno."

msgid "- Recipes are time-based, punching faster does not speed up."
msgstr "- Receptoj estas temp-bazitaj, pugnobati pli rapide ne rapidas."

msgid "- Sneak+drop to count out single items from stack."
msgstr "- Ŝteliri+faligi por kalkuli unuopajn erojn de stako."

msgid "- Some recipes require "pummeling" a node."
msgstr "- Luj receptoj postulas \"frapi\" nodon."

msgid "- Some recipes use a 3x3 "grid", laid out flat on the ground."
msgstr "- Luj receptoj uzas 3x3 \"kradon\", metitan plata sur la tero."

msgid "- Stacks may be pummeled, exact item count may matter."
msgstr "- Stakoj povas esti frapitaj, preciza ero-kalkulo povas gravi."

msgid "- The game is challenging by design, sometimes frustrating. DON'T GIVE UP!"
msgstr "- La ludo estas malfacila laŭ projekto, foje frustranta. NE RENDU!"

msgid "- There is NO inventory screen."
msgstr "- NE estas stokregistro."

msgid "- To pummel, punch a node repeatedly, WITHOUT digging."
msgstr "- Por frapi, pugnobati nodon plurfoje, SEN fosante."

msgid "- Wielded item, target face, and surrounding nodes may matter."
msgstr "- Povita elemento, celvizaĝo kaj ĉirkaŭaj nodoj povas gravi."

msgid "- You do not have to punch very fast (about 1 per second)."
msgstr "- Vi ne devas pugnobati tre rapide (ĉirkaŭ 1 sekunde)."

msgid "@1 (100@2)"
msgstr "@1 (100@2)"

msgid "@1 (10@2)"
msgstr "@1 (10@2)"

msgid "@1 (2)"
msgstr "@1 (2)"

msgid "@1 (20@2)"
msgstr "@1 (20@2)"

msgid "@1 (3)"
msgstr "@1 (3)"

msgid "@1 (30@2)"
msgstr "@1 (30@2)"

msgid "@1 (4)"
msgstr "@1 (4)"

msgid "@1 (40@2)"
msgstr "@1 (40@2)"

msgid "@1 (5)"
msgstr "@1 (5)"

msgid "@1 (50@2)"
msgstr "@1 (50@2)"

msgid "@1 (6)"
msgstr "@1 (6)"

msgid "@1 (60@2)"
msgstr "@1 (60@2)"

msgid "@1 (7)"
msgstr "@1 (7)"

msgid "@1 (70@2)"
msgstr "@1 (70@2)"

msgid "@1 (8)"
msgstr "@1 (8)"

msgid "@1 (80@2)"
msgstr "@1 (80@2)"

msgid "@1 (9)"
msgstr "@1 (9)"

msgid "@1 (90@2)"
msgstr "@1 (90@2)"

msgid "@1 ....."
msgstr "@1 ....."

msgid "@1 |...."
msgstr "@1 |...."

msgid "@1 ||..."
msgstr "@1 ||..."

msgid "@1 |||.."
msgstr "@1 |||.."

msgid "@1 ||||."
msgstr "@1 ||||."

msgid "@1 |||||"
msgstr "@1 |||||"

msgid "About"
msgstr "Pri"

msgid "Active Lens"
msgstr "Aktiva Lenso"

msgid "Active Prism"
msgstr "Aktiva Prismo"

msgid "Adobe"
msgstr "Adobo"

msgid "Adobe Bricks"
msgstr "Adobaj Brikoj"

msgid "Adobe Mix"
msgstr "Adoba Miksaĵo"

msgid "Aggregate"
msgstr "Entuta"

msgid "Air"
msgstr "Aero"

msgid "Amalgamation"
msgstr "Kunfandiĝo"

msgid "Annealed Lode"
msgstr "Kalcinita Ercvejno"

msgid "Annealed Lode Adze"
msgstr "Kalcinita Ercvejno Adzo"

msgid "Annealed Lode Bar"
msgstr "Kalcinita Ercvejno Fandaĵo"

msgid "Annealed Lode Hatchet"
msgstr "Kalcinita Ercvejno Hakilo"

msgid "Annealed Lode Mallet"
msgstr "Kalcinita Ercvejno Maleo"

msgid "Annealed Lode Mallet Head"
msgstr "Kalcinita Ercvejno Maleo Kapo"

msgid "Annealed Lode Hatchet Head"
msgstr "Kalcinita Ercvejno Hakil Kapo"

msgid "Annealed Lode Mattock"
msgstr "Kalcinita Ercvejno Naturo"

msgid "Annealed Lode Mattock Head"
msgstr "Kalcinita Ercvejno Naturo Kapo"

msgid "Annealed Lode Pick"
msgstr "Kalcinita Ercvejno Pioĉo"

msgid "Annealed Lode Pick Head"
msgstr "Kalcinita Ercvejno Pioĉo Kapo"

msgid "Annealed Lode Prill"
msgstr "Kalcinita Ercvejno Prilo"

msgid "Annealed Lode Rake"
msgstr "Kalcinita Ercvejno Rastilo"

msgid "Annealed Lode Rod"
msgstr "Kalcinita Ercbastono"

msgid "Annealed Lode Spade"
msgstr "Kalcinita Ercvejno Fosilo"

msgid "Annealed Lode Spade Head"
msgstr "Kalcinita Ercvejno Fosilo Kapo"

msgid "Ash"
msgstr "Cindro"

msgid "Ash Lump"
msgstr "Cindero Peco"

msgid "Bindy"
msgstr "Bindi"

msgid "Bindy Adobe"
msgstr "Bindi Adobo"

msgid "Bindy Pliant Adobe"
msgstr "Bindi Fleksebla Adobo"

msgid "Bindy Pliant Stone"
msgstr "Bindi Fleksebla Stono"

msgid "Bindy Stone"
msgstr "Bindi Stono"

msgid "Bindy Tarstone"
msgstr "Bindi Gudristono"

msgid "Blank"
msgstr "Malplena"

msgid "Bonded Adobe Brick Hinged Panel"
msgstr "Ligita Adobo Brikon Ĉarniran Panelon"

msgid "Bonded Adobe Bricks"
msgstr "Ligitajn Adobo Brikojn"

msgid "Bonded Adobe Brick Panel"
msgstr "Ligita Adobo Brikon Panelon"

msgid "Bonded Sandstone Brick Hinged Panel"
msgstr "Ligita Sabloŝtonon Brikon Ĉarniran Panelon"

msgid "Bindy Pliant Sandstone"
msgstr "Bindi Fleksiĝema Sabloŝtono"

msgid "Bindy Pliant Tarstone"
msgstr "Bindi Fleksiĝema Gudristono"

msgid "Bindy Sandstone"
msgstr "Bindi Sabloŝtono"

msgid "Bonded Sandstone Brick Panel"
msgstr "Ligita Sabloŝtonon Brikon Panelon"

msgid "Bonded Sandstone Bricks"
msgstr "Ligita Sabloŝtonon Brikojn"

msgid "Bonded Stone Brick Hinged Panel"
msgstr "Ligita Ŝtonon Brikon Ĉarniran Panelon"

msgid "Bonded Stone Brick Panel"
msgstr "Ligita Ŝtonon Brikon Panelon"

msgid "Bonded Stone Bricks"
msgstr "Ligita Ŝtonon Brikojn"

msgid "Bonded Tarstone Brick Hinged Panel"
msgstr "Ligita Gudroŝtono Brikon Ĉarniran Panelon"

msgid "Bonded Tarstone Brick Panel"
msgstr "Ligita Gudroŝtono Brikon Panelon"

msgid "Bonded Tarstone Bricks"
msgstr "Ligitajn Gudristono Brikojn"
