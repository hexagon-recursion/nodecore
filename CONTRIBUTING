========================================================================
------------------------------------------------------------------------

For your contribution to be included in this project, you must
understand and agree to the terms in this document at the time of
contribution.

- A "contribution" consists of all files, patches and changes, source
  control commits, comments and other metadata submitted to the project
  maintainer(s) for inclusion or other use in the project.

- All contributions must consist only of your own original work, to
  which you retain copyright, or to which copyright is not applicable.

- All copyrightable portions must be non-exclusively, perpetually and
  irrevocably licensed under the same license terms as the overall
  project.

- You will receive attribution in the project's license, in the form of
  "Portions (C) ...", which shall cover all portions of all
  contributions.  You agree that this constitutes adequate attribution
  under the terms of the license.

- You may request to be attributed pseudonymously immediately before or
  at the time of submission.  Unless otherwise specified, source control
  metadata will be used for attribution.

........................................................................

Other Guidelines:

- For translators: Weblate defaults to using your full name and email
  as entered during registration for attribution.  "Time of submission"
  is as soon as you start translating any strings.  If you want a
  different attribution be sure to request it BEFORE you start
  translation work.

- Please do not use the nc_* namespace for projects you will be
  releasing directly to the public, e.g. via ContentDB, Forums, etc.
  This namespace should only be for things that will be included in
  the base game.

- Non-included contributions to the project, such as hosting or other
  services, content such as videos/streams, screenshots/galleries,
  reviews, stories and other publicity do not need to follow the
  guidelines above unless they are intended to also be eligible for
  inclusion directly in the project.

- Keep original media source files that are not directly used by the
  game engine (e.g. *.blend, *.xcf, *.svg) in folders named "src" in the
  appropriate mod/area.  These will be distributed with the source
  repo, but will NOT be packaged by ContentDB to keep download sizes
  minimal.

- Lua code formatting/style is using:
        https://gitlab.com/Warr1024/luatools
  Install the commands in your $PATH, and then in the root of the
  project, run this command to format the code:
        lualocals -f luaintraspace -f pkluaformat
  You can add a -c option to lualocals to make it check only but not
  modify your code; the check-only version is very useful in a git
  pre-commit hook script.

------------------------------------------------------------------------
========================================================================
